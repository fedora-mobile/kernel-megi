Name:           megi-kernel
Version:        5.10.9
Release:        1%{?dist}
Summary:        latest megi kernel for the pinephone 

License:        GPLv3+
URL:            https://megous.com/git/linux/log/?h=orange-pi-5.10
Source0:        https://xff.cz/kernels/5.10/pp.tar.gz
Source1:        boot.cmd
Source2:        boot.scr

Obsoletes:      megi-kernel-bh
Obsoletes:      megi-kernel-ce

%description
Megis version of the Linux Kernel, for PinePhone.

%global debug_package %{nil}

%prep
%autosetup -p1 -n pp-5.10


%install
mkdir -p $RPM_BUILD_ROOT/boot/
cp Image $RPM_BUILD_ROOT/boot/
cp %{SOURCE1} $RPM_BUILD_ROOT/boot/
cp %{SOURCE2} $RPM_BUILD_ROOT/boot/
mkdir -p $RPM_BUILD_ROOT/boot/allwinner
cp board-1.1.dtb $RPM_BUILD_ROOT/boot/allwinner/sun50i-a64-pinephone-1.1.dtb
cp board-1.2.dtb $RPM_BUILD_ROOT/boot/allwinner/sun50i-a64-pinephone-1.2.dtb
cp pinetab.dtb $RPM_BUILD_ROOT/boot/allwinner/sun50i-a64-pinetab.dtb
mkdir -p $RPM_BUILD_ROOT/lib/modules/
cp -r modules/lib/modules/* $RPM_BUILD_ROOT/lib/modules/

%files
/boot/Image
/boot/boot.cmd
/boot/boot.scr
/lib/modules/
/boot/allwinner/sun50i-a64-pinephone-1.1.dtb
/boot/allwinner/sun50i-a64-pinephone-1.2.dtb
/boot/allwinner/sun50i-a64-pinetab.dtb

%changelog
* Fri Jan 22 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.9-1
- Update to latest

* Tue Jan 12 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.4-2
- Don't require device specific packages (phone vs tablet)

* Fri Jan 01 2021 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.4-1
- Upgrade to 5.10.4

* Wed Dec 03 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.rc6b-4
- Upgrade to 5.10-rc6b

* Tue Dec 01 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.10.rc6-3
- Upgrade to 5.10-rc6

* Sat Oct 31 2020 Nikhil Jha <hi@nikhiljha.com> - 5.10-2
- Upgrade to 5.10 as of oct31

* Fri Oct 16 2020 Yoda - 5.9.0 GA
- Upgrade to 5.9.0 GA
- New audio driver - requires new pineaudio util
- New kernel layout - requires new uboot

* Fri Aug 28 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.9-2
- Upgrade to 5.9 rc2

* Fri Aug 21 2020 Torrey Sorensen <torbuntu@fedoraproject.org> - 5.9-1
- Upgrade to 5.9

* Thu Aug 06 2020 Nikhil Jha <hi@nikhiljha.com> - 5.8-1
- Upgrade to 5.8

* Mon Jun 15 2020 Nikhil Jha <hi@nikhiljha.com> - 5.7-1
- Initial packaging (hack)
